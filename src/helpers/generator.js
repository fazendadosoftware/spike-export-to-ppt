import PptxGenJS from 'pptxgenjs'

export const getSlideLayoutOptions = () => {
  return [
    { value: 'LAYOUT_16x9', width: 10, height: 5.625, label: '16x9' },
    { value: 'LAYOUT_16x10', width: 10, height: 6.25, label: '16x10' },
    { value: 'LAYOUT_4x3', width: 10, height: 7.5, label: '4x3' },
    { value: 'LAYOUT_WIDE', width: 13.3, height: 7.5, label: 'Wide' },
    { value: 'LAYOUT_USER', label: 'User defined' }
  ]
}

export const generatePowerPointFromExportContainer = (containerEl, layout = { value: 'LAYOUT_16x9' }) => {
  if (!isElement(containerEl)) throw Error('invalid container element')
  if (containerEl.className.split(' ').filter(c => c === 'export-container').length !== 1) throw Error(`invalid element attribute passed, expected "export-container" element`)

  // Get container origin
  const { left, top, width, height } = containerEl.getBoundingClientRect()
  const origin = { x0: window.scrollX + left, y0: window.scrollY + top, width, height }

  const cardsContainer = containerEl.querySelector('.cards-container')
  if (!cardsContainer) throw Error('expected ".export-container" element to have a ".cards-container" child element')
  const { children } = cardsContainer // the card elements
  const pptx = new PptxGenJS()
  pptx.setLayout(layout.value === 'LAYOUT_USER' ? { name: 'custom_layout', width: layout.width, height: layout.height } : layout.value)
  const slide = pptx.addNewSlide()
  Array.from(children).forEach((child, i) => generateCardShape(child, origin, slide))
  pptx.save('Sample Presentation')
}

export const generateCardShape = (el, origin, slide) => {
  if (!isElement(el)) throw Error('invalid container element')
  if (el.className.split(' ').filter(c => ['card'].indexOf(c) > -1).length !== 1) throw Error(`invalid element attribute passed, expected el to have ".card"`)
  const pptx = new PptxGenJS()

  let { left, top, width, height } = el.getBoundingClientRect() // coordinates are relative to browser viewport, will change when window scrolls
  const x0 = `${(window.scrollX + left - origin.x0) * 100 / origin.width}%`
  const y0 = `${(window.scrollY + top - origin.y0) * 100 / origin.height}%`
  width = `${width * 100 / origin.width}%`
  height = `${height * 100 / origin.height}%`

  const headerEl = el.querySelector('div.header')
  const childEl = el.querySelector('div.collapsed.section')
  let textContent = isElement(headerEl) ? '' : el.textContent.trim()

  // outer container
  let { backgroundColor, color, fontSize } = getComputedStyle(isElement(headerEl) ? el : childEl)
  fontSize = Number(fontSize.replace(/px/g, '')) - 4
  backgroundColor = rgbToHex(backgroundColor)
  color = rgbToHex(color)
  const container = [ textContent, { shape: pptx.shapes.ROUNDED_RECTANGLE, x: x0, y: y0, w: width, h: height, line: 'd3d3d3', lineSize: 0.25, fill: backgroundColor, rectRadius: 0.04, align: 'center', fontSize, color, shrinkText: true } ]
  if (!isElement(headerEl)) slide.addText(...container)

  // header
  if (isElement(headerEl)) {
    let headerShape = []
    let { textContent } = headerEl
    textContent = textContent.trim()
    let { backgroundColor, color, fontSize } = getComputedStyle(headerEl)
    fontSize = Number(fontSize.replace(/px/g, '')) - 4
    backgroundColor = rgbToHex(backgroundColor)
    color = rgbToHex(color)
    let { left, top, width, height } = headerEl.getBoundingClientRect() // coordinates are relative to browser viewport, will change when window scrolls
    const headerHeight = height
    // Compute absolute locations
    const x0 = `${(window.scrollX + left - origin.x0) * 100 / origin.width}%`
    const y0 = `${(window.scrollY + top - origin.y0) * 100 / origin.height}%`
    width = `${width * 100 / origin.width}%`
    height = `${height * 100 / origin.height}%`
    headerShape = [textContent, { shape: pptx.shapes.ROUND_2_SAME_RECTANGLE, x: x0, y: y0, w: width, h: height, line: 'd3d3d3', lineSize: 0.25, fill: backgroundColor, align: 'c', fontSize, color }]
    // body
    const bodyEl = el.querySelector('div.body')
    if (isElement(bodyEl)) {
      let { left, width, height } = bodyEl.getBoundingClientRect() // coordinates are relative to browser viewport, will change when window scrolls
      const x0 = `${(window.scrollX + left - origin.x0) * 100 / origin.width}%`
      width = `${width * 100 / origin.width}%`
      height = `${(height + headerHeight) * 100 / origin.height}%`
      const bodyContainer = [ pptx.shapes.ROUNDED_RECTANGLE, { x: x0, y: y0, w: width, h: height, line: 'd3d3d3', lineSize: 0.25, fill: 'f3f3f3', rectRadius: 0.04 } ]
      slide.addShape(...bodyContainer)
      const { children } = bodyEl // the card elements
      Array.from(children).forEach((child, i) => generateCardShape(child, origin, slide))
    }
    slide.addText(...headerShape)
  }
  return slide
}

// Returns true if it is a DOM element
export const isElement = o => typeof HTMLElement === 'object' ? o instanceof HTMLElement : o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName === 'string'

const rgbToHex = rgb => {
  const matchColors = /rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)/
  const match = matchColors.exec(rgb)
  if (match.length === 4) {
    // eslint-disable-next-line
    const [ _, r, g, b ] = match
    return [r, g, b].map(color => { const hex = Number(color).toString(16); return hex.length < 2 ? `0${hex}` : hex }).join('')
  }
  return 'ffffff'
}
